<?php

/**
 * Functions and Definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enqueuing of scripts and styles.
 */
function sicompra_scripts() {    
    wp_enqueue_style( 'slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', array(), '1.9.0' );
    wp_enqueue_style( 'slick-theme', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array( 'slick' ), '1.9.0' );
    wp_enqueue_style( 'sicompra-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'sicompra-reset-styles', get_template_directory_uri() . '/assets/css/shared/reset.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'sicompra-gutenberg-styles', get_template_directory_uri() . '/assets/css/shared/gutenberg.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'sicompra-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/styles.css', array(), wp_get_theme()->get( 'Version' ) );    

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), '3.5.1', true );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array( 'jquery' ), '1.9.0', true );
    wp_enqueue_script( 'sicompra-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-scripts.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'sicompra_scripts');

/** 
 * Set theme defaults and register support for various WordPress features.
 */
function sicompra_setup() {
    // Enabling translation support
    $textdomain = 'sicompra';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 61,
        'width'       => 220,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Customizable header image
    add_theme_support( 'custom-header', array(
        'width'         => 2560,
        'height'        => 150,
        'flex-width'    => true,
        'flex-height'   => true,
    ) );


    // Menu registration
    register_nav_menus(array(
        'main_menu' => __( 'Main Menu', 'sicompra' ),
        'footer_menu' => __( 'Footer Menu', 'sicompra' ),
    ));

    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/webfonts.css' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/reset.css' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/gutenberg.css' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
	add_theme_support( 'wp-block-styles' );

    // Disable custom colors
    add_theme_support( 'disable-custom-colors' );

    // Creates the specific color palette
    add_theme_support( 'editor-color-palette', array(
        array(
            'name'  => __( 'White', 'sicompra' ),
            'slug'  => 'white',
            'color'    => '#ffffff',
        ),
        array(
            'name'  => __( 'Black', 'sicompra' ),
            'slug'  => 'black',
            'color'    => '#000000',
        ),
        array(
            'name'  => __( 'Red', 'sicompra' ),
            'slug'  => 'red',
            'color'    => '#ed2227',
        ),
        array(
            'name'  => __( 'Mikado Yellow', 'sicompra' ),
            'slug'  => 'mikado-yellow',
            'color'    => '#ffc427',
        ),
        array(
            'name'  => __( 'YInMn Blue', 'sicompra' ),
            'slug'  => 'yinmn-blue',
            'color'    => '#30519b',
        ),
        array(
            'name'  => __( 'Lime Green', 'sicompra' ),
            'slug'  => 'lime-green',
            'color'    => '#00c417',
        ),
    ) );

    // Disable custom gradients
    add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'sicompra_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sicompra_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Footer #1', 'sicompra'),
        'id' => 'sicompra-sidebar-footer-1',
        'description' => __( 'The widgets in this area will be displayed in the first column in Footer.', 'sicompra' ),
    ) ) );

    // Footer #2 
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Footer #2', 'sicompra'),
        'id' => 'sicompra-sidebar-footer-2',
        'description' => __( 'The widgets in this area will be displayed in the second column in Footer.', 'sicompra' ),
    ) ) );

    // Footer #3
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Footer #3', 'sicompra'),
        'id' => 'sicompra-sidebar-footer-3',
        'description' => __( 'The widgets in this area will be displayed in the third column in Footer.', 'sicompra' ),
    )));

    // Footer #4
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Footer #4', 'sicompra'),
        'id' => 'sicompra-sidebar-footer-4',
        'description' => __( 'The widgets in this area will be displayed in the fourth column in Footer.', 'sicompra' ),
    )));
}

add_action( 'widgets_init', 'sicompra_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 * Custom Post-Types
 */
require_once get_template_directory() . '/includes/post-types.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';

/**
 * Meta Box Framework - Custom Fields Config
 */
require_once get_template_directory() . '/includes/metabox-custom-fields.php';



/**
 * Add tempalte part shortcode
 */
function get_carousel_slide( $atts ) {
    ob_start();
    get_template_part( 'partials/carousel/carousel', 'slide' );
    return ob_get_clean();
}
add_shortcode( 'carousel-slide', 'get_carousel_slide' );

function get_carousel_testimonials( $atts ) {
    ob_start();
    get_template_part( 'partials/carousel/carousel', 'testimonials' );
    return ob_get_clean();
}
add_shortcode( 'carousel-testimonials', 'get_carousel_testimonials' );