<?php
$slides = get_theme_mod( 'setting_carousel' );
$is_mobile = wp_is_mobile();

if ($slides) : 
?>
    
<div class="carousel-slide">
    <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
        <img 
            class=""
            src="<?php echo wp_get_attachment_image_src(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], 'full')[0]; ?>" 
            alt="<?php echo get_post_meta(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], '_wp_attachment_image_alt', TRUE); ?>"
        >
    <?php endfor; ?>
</div>

<?php endif; ?>