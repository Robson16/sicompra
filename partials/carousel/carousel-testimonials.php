<!-- BEGIN TESTIMONIALS CAROUSEL -->
<?php
$testimonials = new WP_Query(array(
    'post_type' => 'testimonials',
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
    'nopaging' => true,
));

if ($testimonials->have_posts()) :
?>
    <div class="testimonials">
        <div class="carousel-testimonials">
            <?php 
            while ( $testimonials->have_posts() ) : $testimonials->the_post();
            ?>
                <div class="testimony">

                    <p class="testimony-text"><?php echo get_the_content();?></p>

                    <div class="testimony-author">
                        <div class="testimony-thumbnail-frame">
                            <?php 
                                if ( has_post_thumbnail() ) { 
                                    the_post_thumbnail( 'thumbnail', array(
                                        'class' => 'testimony-thumbnail',
                                        'title' => get_the_title(),
                                        'alt' => get_the_title(),
                                    ) );
                                }
                            ?>
                        </div>
                        <!-- /.testimony-thumbnail-frame -->
                        <div class="testimony-titles">
                            <h3 class="testimony-title"><?php echo get_the_title();?></h3>
                            <h4 class="testimony-subtitle"><?php echo rwmb_meta( 'sicompra-testimonials-subtitle' ); ?></h4>
                        </div>
                        <!-- /.testimony-titles -->
                    </div>
                    <!-- /.testimony-author -->

                </div>
                <!-- /.testimony -->
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
        <!-- /#testimonials-corousel -->
    </div>
    <!-- /.testimonials -->
<?php endif; ?>
<!-- END TESTIMONIALS CAROUSEL -->