<?php

/**
 * Front-Page template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
    <?php
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            echo "<div class='entry-content'>";
            the_content();
            echo "</div><!-- .entry-content -->";
        }
    }
    ?>
</main>

<?php
get_footer();