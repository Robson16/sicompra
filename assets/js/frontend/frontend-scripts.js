/**
 * Front-End JS
 */

jQuery(document).ready(function ($) {
    $('.navbar-toggler').click(function () {
        let target = $(this).data('target');
        $(target).toggleClass('show');
    });

    $('.menu-item-has-children > a').click(function (event) {
        event.preventDefault();
    });

    $('.carousel-slide').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $('.carousel-testimonials').slick({
        autoplay: true,
        autoplaySpeed: 7000,
    });
});