<?php

/**
 * Meta Box – WordPress Custom Fields Framework Configs
 * 
*/

function sicompra_get_meta_box( $meta_boxes ) {
	$prefix = 'sicompra-';

	$meta_boxes[] = array(
		'id' => 'testimonials-additional_information',
		'title' => esc_html__( 'Additional informations', 'sicompra' ),
		'post_types' => array( 'testimonials' ),
		'context' => 'after_title',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'testimonials-subtitle',
				'type' => 'text',
				'name' => esc_html__( 'Sub Title', 'sicompra' ),
			),
		),
	);

	return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'sicompra_get_meta_box' );