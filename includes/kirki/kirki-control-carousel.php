<?php

Kirki::add_section( 'section_carousel', array(
    'title' => esc_html__( 'Carrossel', 'sicompra' ),
    'priority' => 160,
));

Kirki::add_field( 'sicompra_kirki_config', [
    'type' => 'repeater',
    'label' => esc_html__( 'Slides', 'sicompra' ),
    'section' => 'section_carousel',
    'priority' => 10,
    'row_label' => [
        'type' => 'field',
        'value' => esc_html__( 'Slide', 'sicompra' ),
        'field' => 'link_text',
    ],
    'button_label' => esc_html__( 'Add new', 'sicompra' ),
    'settings' => 'setting_carousel',
    'fields' => [
        'slide_desktop' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide', 'sicompra' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
        'slide_mobile' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide Mobile', 'sicompra' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
    ]
]);
