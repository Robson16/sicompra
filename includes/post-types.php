<?php

// Register Custom Post Type
function sicompra_testimonials() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'sicompra' ),
        'singular_name'         => _x( 'Testimony', 'Post Type Singular Name', 'sicompra' ),
        'menu_name'             => __( 'Testimonials', 'sicompra' ),
        'name_admin_bar'        => __( 'Testimonials', 'sicompra' ),
        'archives'              => __( 'Item Archives', 'sicompra' ),
		'attributes'            => __( 'Item Attributes', 'sicompra' ),
		'parent_item_colon'     => __( 'Parent Item:', 'sicompra' ),
		'all_items'             => __( 'All Items', 'sicompra' ),
		'add_new_item'          => __( 'Add New Item', 'sicompra' ),
		'add_new'               => __( 'Add New', 'sicompra' ),
		'new_item'              => __( 'New Item', 'sicompra' ),
		'edit_item'             => __( 'Edit Item', 'sicompra' ),
		'update_item'           => __( 'Update Item', 'sicompra' ),
		'view_item'             => __( 'View Item', 'sicompra' ),
		'view_items'            => __( 'View Items', 'sicompra' ),
		'search_items'          => __( 'Search Item', 'sicompra' ),
		'not_found'             => __( 'Not found', 'sicompra' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sicompra' ),
		'featured_image'        => __( 'Featured Image', 'sicompra' ),
		'set_featured_image'    => __( 'Set featured image', 'sicompra' ),
		'remove_featured_image' => __( 'Remove featured image', 'sicompra' ),
		'use_featured_image'    => __( 'Use as featured image', 'sicompra' ),
		'insert_into_item'      => __( 'Insert into item', 'sicompra' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'sicompra' ),
		'items_list'            => __( 'Items list', 'sicompra' ),
		'items_list_navigation' => __( 'Items list navigation', 'sicompra' ),
		'filter_items_list'     => __( 'Filter items list', 'sicompra' ),
    );
    $args = array(
        'label'                 => __( 'Testimony', 'sicompra' ),
        'description'           => __( 'Customer testimonial', 'sicompra' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-quote',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'testimonials', $args );

}
add_action( 'init', 'sicompra_testimonials', 0 );