<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<footer id="footer">
    <div class="footer-widgets">
        <div class="container">
            <div>
                <?php if ( is_active_sidebar( 'sicompra-sidebar-footer-1' ) ) dynamic_sidebar( 'sicompra-sidebar-footer-1' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'sicompra-sidebar-footer-2' ) ) dynamic_sidebar( 'sicompra-sidebar-footer-2' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'sicompra-sidebar-footer-3' ) ) dynamic_sidebar( 'sicompra-sidebar-footer-3' ); ?>
            </div>

            <div>
                <?php if ( is_active_sidebar( 'sicompra-sidebar-footer-4' ) ) dynamic_sidebar( 'sicompra-sidebar-footer-4' ); ?>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <span class="text-white">
            <?php _e( 'Design by', 'sicompra'); ?>&nbsp;<a href="https://novahorda.com/" target="_blank" rel="noopener">NHRD</a>&nbsp;&copy;&nbsp;<?php echo bloginfo('title') . ', ' . wp_date('Y'); ?>
            </span>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'footer_menu',
                'depth' => 1,
                'container_class' => 'footer-menu-wrap',
                'menu_class' => 'footer-menu',
            ) );
            ?>
        </div>
    </div>
</footer>  

<?php wp_footer(); ?>

</body>

</html>
