<?php
/**
 * The template for displaying 404 page not found.
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
    <section class="container">
        <h1 class="page-title"><?php _e("Can't find this page", "sicompra"); ?></h1>
        <p><?php _e('It looks like nothing was found at this location', 'sicompra'); ?></p>
        <a href="<?php echo get_home_url(); ?>"><?php _e( 'Return to home page' ,'sicompra' ); ?></a>
    </section>
</main>

<?php
get_footer();
